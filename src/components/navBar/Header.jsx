import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-rufous bg-opacity-75'>
      <div className='container-fluid'>
        <Link className='navbar-brand badge bg-slimy text-wrap  fs-4 border border-4' to='/'>
          RICK and MORTY
        </Link>
        <div className='collapse navbar-collapse' id='navbarText'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link className='nav-link active fs-4 ' aria-current='page' to='/'>
                HOME
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
