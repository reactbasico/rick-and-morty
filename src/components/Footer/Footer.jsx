export function Footer() {
  return (
    <div className='navbar-dark bg-rufous bg-opacity-75 fixed-bottom'>
      <p className='text-center text-white py-4 mb-0'>
        Proyecto creado por Luis Payares Joly, CAR-IV
      </p>
    </div>
  );
}
