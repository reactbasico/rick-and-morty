import '../../assets/css/App.css';
export function Buscador({ valor, onBuscador }) {
  return (
    <div className='d-flex justify-content-end'>
      <p className='text-sheeny py-5 px-3 fs-3 fw-bolder '>Buscar personaje : </p>
      <div className='mb-3 col-5'>

        <input
          type='text'
          className=' mt-5 form-control text-white bg-gCyan'
          //placeholder='Buscar personaje...'
          value={valor}
          onChange={(evento) => onBuscador(evento.target.value)}
        />
      </div>
    </div>
  );
}
